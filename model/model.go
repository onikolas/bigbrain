package model

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"os"
	"time"

	"github.com/google/uuid"
)

type Model struct {
	Data []Node
	root string
}

// Init a model at a given root folder. If the folder is empty it will create necessary files as
// needed.
func (m *Model) Init(root string) error {
	m.root = root
	db := root + "/db/db.json"

	// db exists
	if bytes, err := ioutil.ReadFile(db); err == nil {
		if err = json.Unmarshal(bytes, m); err != nil {
			return err
		}
		return nil
	}

	// no db - create it
	if err := os.Mkdir(root+"/db", 0755); err != nil {
		return err
	}

	b, err := json.Marshal(m)
	if err != nil {
		return err
	}
	if err = ioutil.WriteFile(db, b, 0644); err != nil {
		return err
	}

	return nil
}

func (m *Model) Save() error {
	db := m.root + "/db/db.json"
	bytes, err := json.Marshal(m)
	if err == nil {
		if err = os.WriteFile(db, bytes, 0644); err != nil {
			return err
		}
		return nil
	}
	return err
}

func (m *Model) Add(n Node) error {
	n.ID, _ = uuid.NewRandom()
	n.DateAdded = time.Now()
	m.Data = append(m.Data, n)
	return m.Save()
}

// Like Add but keep the existing DateAdded param.
func (m *Model) AddExisting(n Node) error {
	n.ID, _ = uuid.NewRandom()
	m.Data = append(m.Data, n)
	return m.Save()
}

func (m *Model) GetNode(id uuid.UUID) (Node, error) {
	for i := range m.Data {
		if m.Data[i].ID == id {
			return m.Data[i], nil
		}
	}
	return Node{}, errors.New("Not found")
}

func (m *Model) GetNodes(filter func(Node) bool) []Node {
	nodes := []Node{}
	for _, v := range m.Data {
		if filter(v) {
			nodes = append(nodes, v)
		}
	}
	return nodes
}

func (m *Model) MarkDone(id uuid.UUID) error {
	for i := range m.Data {
		if m.Data[i].ID == id {
			m.Data[i].Done = true
			return m.Save()
		}
	}
	return errors.New("id not found")
}

func (m *Model) Delete(id uuid.UUID) error {
	for i := range m.Data {
		if m.Data[i].ID == id {
			s := m.Data
			s[len(s)-1], s[i] = s[i], s[len(s)-1]
			m.Data = s[:len(s)-1]
			break
		}
	}
	return m.Save()
}

func (m *Model) Replace(n Node) error {
	for i := range m.Data {
		if m.Data[i].ID == n.ID {
			m.Data[i] = n
			break
		}
	}
	return m.Save()
}
