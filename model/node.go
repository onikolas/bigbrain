package model

import (
	"fmt"
	"reflect"
	"time"

	"github.com/google/uuid"
)

type Node struct {
	ID             uuid.UUID
	Title          string
	DateAdded      time.Time
	Deadline       time.Time
	EffortEstimate time.Duration
	Description    string
	Tags           []string
	FileLink       string
	Children       []uuid.UUID
	Done           bool
	Ext            map[string]any
}

func (n Node) Strings() []string {
	s := []string{}
	v := reflect.Indirect(reflect.ValueOf(n))
	for i := 0; i < v.NumField(); i++ {
		s = append(s, fmt.Sprint(v.Field(i)))
	}
	return s
}

func (n Node) FieldNames() []string {
	s := []string{}
	v := reflect.Indirect(reflect.ValueOf(n))
	for i := 0; i < v.NumField(); i++ {
		s = append(s, v.Type().Field(i).Name)
	}
	return s
}
