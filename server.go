package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"time"

	"github.com/google/uuid"
	"gitlab.com/onikolas/bigbrain/connectors/asana"
	"gitlab.com/onikolas/bigbrain/model"
)

type Server struct {
	Model *model.Model
}

func (s *Server) Init(model *model.Model) {
	s.Model = model
	s.MapModelToEndpoints()
	s.Serve()
}

func (s *Server) MapModelToEndpoints() {
	http.HandleFunc("/status", s.getStatus)
	http.HandleFunc("/favicon.ico", s.favicon)
	http.HandleFunc("/add", s.addNode)
	http.HandleFunc("/", s.list)
	http.HandleFunc("/template", s.nodeTemplate)
	http.HandleFunc("/markdone", s.markDone)
	http.HandleFunc("/delete", s.delete)
	http.HandleFunc("/get", s.get)
	http.HandleFunc("/replace", s.replace)
	http.HandleFunc("/list", s.list)
	http.HandleFunc("/asanasync", s.asanaSync)
}

func (s *Server) Serve() {
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%v", Settings.Port), nil))
}

func (s *Server) getStatus(w http.ResponseWriter, req *http.Request) {
	io.WriteString(w, "runnning")
}

func (s *Server) addNode(w http.ResponseWriter, req *http.Request) {
	n := model.Node{}
	bodyBytes, err := ioutil.ReadAll(req.Body)
	if err != nil {
		io.WriteString(w, err.Error())
		return
	}
	if err := json.Unmarshal(bodyBytes, &n); err == nil {
		if err := s.Model.Add(n); err != nil {
			io.WriteString(w, err.Error())
			return
		}
		io.WriteString(w, "OK\n")
	} else {
		io.WriteString(w, err.Error())
		fmt.Println(err)
	}
}

func (s *Server) nodeTemplate(w http.ResponseWriter, req *http.Request) {
	node := model.Node{
		ID:             uuid.New(),
		Title:          "Template node",
		DateAdded:      time.Now(),
		Deadline:       time.Now().Add(time.Hour),
		EffortEstimate: time.Minute * 12,
		Description:    "This is a template node.",
		Tags:           []string{"Meeting"},
		FileLink:       "template.md",
	}

	byte, _ := json.Marshal(node)

	if req.ParseForm() == nil {
		if req.FormValue("flat") != "" {
			w.Write(byte)
			return
		}
	}

	var formatted bytes.Buffer
	json.Indent(&formatted, byte, "", "  ")
	w.Write(formatted.Bytes())
}

func (s *Server) favicon(w http.ResponseWriter, req *http.Request) {
	http.ServeFile(w, req, "html/favicon.ico")
}

func (s *Server) markDone(w http.ResponseWriter, req *http.Request) {
	if req.ParseForm() == nil && req.FormValue("id") != "" {
		uid, err := uuid.ParseBytes([]byte(req.FormValue("id")))
		if err != nil {
			io.WriteString(w, err.Error())
			return
		}
		if err = s.Model.MarkDone(uid); err != nil {
			io.WriteString(w, err.Error())
			return
		}
		io.WriteString(w, "OK")
		return
	}
	io.WriteString(w, "No id provided")
}

func (s *Server) delete(w http.ResponseWriter, req *http.Request) {
	if req.ParseForm() == nil && req.FormValue("id") != "" {
		uid, err := uuid.ParseBytes([]byte(req.FormValue("id")))
		if err != nil {
			io.WriteString(w, err.Error())
			return
		}
		s.Model.Delete(uid)
		io.WriteString(w, "OK")
		return
	}
	io.WriteString(w, "No id provided")
}

func (s *Server) get(w http.ResponseWriter, req *http.Request) {
	if req.ParseForm() == nil && req.FormValue("id") != "" {
		uid, err := uuid.ParseBytes([]byte(req.FormValue("id")))
		if err != nil {
			io.WriteString(w, err.Error())
			return
		}
		note, err := s.Model.GetNode(uid)
		if err != nil {
			io.WriteString(w, err.Error())
			return
		}

		byte, _ := json.Marshal(note)
		var formatted bytes.Buffer
		json.Indent(&formatted, byte, "", "  ")
		w.Write(formatted.Bytes())
		return

	}
	io.WriteString(w, "No id provided")
}

func (s *Server) replace(w http.ResponseWriter, req *http.Request) {
	n := model.Node{}
	bodyBytes, err := ioutil.ReadAll(req.Body)
	if err != nil {
		io.WriteString(w, err.Error())
		return
	}
	if err := json.Unmarshal(bodyBytes, &n); err == nil {

		if err := s.Model.Replace(n); err != nil {
			io.WriteString(w, err.Error())
			return
		}
		io.WriteString(w, "OK\n")
	} else {
		io.WriteString(w, err.Error())
		fmt.Println(err)
	}
}

func (s *Server) list(w http.ResponseWriter, req *http.Request) {
	f := func(n model.Node) bool {
		return !n.Done
	}

	nodes := s.Model.GetNodes(f)
	byte, _ := json.Marshal(nodes)
	w.Write(byte)
}

func (s *Server) asanaSync(w http.ResponseWriter, req *http.Request) {
	asana.Authenticate(Settings.AsanaPam)
	tasks, err := asana.GetUsersTasks(Settings.AsanaUser)
	if err != nil {
		return
	}

	for i, v := range tasks {
		fmt.Println("syncing task: ", i, v.Title, v.Done, getAsanaGid(v))

		// check if task already exists
		gid := getAsanaGid(v)
		existing := s.Model.GetNodes(func(n model.Node) bool {
			if gid == getAsanaGid(n) {
				return true
			}
			return false
		})
		if len(existing) > 1 {
			panic("why?")
		}
		if len(existing) == 1 {
			// task exists, update it
			v.ID = existing[0].ID
			s.Model.Replace(v)
		} else if !v.Done {
			// new task (only add new incomplete tasks)
			s.Model.AddExisting(v)
		}
	}
}

func getAsanaGid(n model.Node) string {
	if v, ok := n.Ext["asana"]; ok {
		asanaTask := v.(asana.AsanaTask)
		return asanaTask.Gid
	}
	return ""
}
