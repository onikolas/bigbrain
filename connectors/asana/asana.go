package asana

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"

	"gitlab.com/onikolas/bigbrain/model"
)

var client http.Client
var gid string
var gpat string

func authorizedRequest(url string, requestType string, urlParams map[string]string) *http.Request {
	request, _ := http.NewRequest(requestType, url, nil)
	request.Header.Add("Authorization", "Bearer "+gpat)
	params := request.URL.Query()
	for k, v := range urlParams {
		params.Add(k, v)
	}
	request.URL.RawQuery = params.Encode()
	return request
}

// Authenticate using a personal access token. Token is stored locally.
func Authenticate(pat string) error {
	request, err := http.NewRequest("GET", "https://app.asana.com/api/1.0/users/me/workspaces", nil)
	request.Header.Add("Authorization", "Bearer "+pat)
	response, err := client.Do(request)
	if err != nil {
		return err
	}
	defer response.Body.Close()
	body, err := io.ReadAll(response.Body)
	auth := AsanaAuthorizeResponse{}
	json.Unmarshal(body, &auth)

	gid = auth.Data[0].Gid
	gpat = pat
	fmt.Println("Authenticated to asana", auth)
	return nil
}

func getTaskPage(request *http.Request) (AsanaTasksPage, error) {
	response, err := client.Do(request)
	if err != nil {
		return AsanaTasksPage{}, err
	}
	body, err := io.ReadAll(response.Body)
	response.Body.Close()
	page := AsanaTasksPage{}
	err = json.Unmarshal(body, &page)
	if err != nil {
		return AsanaTasksPage{}, err
	}
	return page, nil
}

// Get a user's tasks. TODO filter the request to fetch after a specific date.
func GetUsersTasks(user string) ([]model.Node, error) {
	url := "https://app.asana.com/api/1.0/tasks"
	params := map[string]string{
		"opt_fields": "gid,created_at,name,notes,tags,completed",
		"assignee":   user,
		"workspace":  gid,
		"limit":      "10",
	}
	request := authorizedRequest(url, "GET", params)

	nodes := []model.Node{}

	for {
		page, err := getTaskPage(request)
		if err != nil {
			break
		}

		for _, v := range page.Data {
			nodes = append(nodes, AsanaTaskToNode(v))
		}

		request = authorizedRequest(page.NextPage.Uri, "GET", nil)
	}

	return nodes, nil
}

func AsanaTaskToNode(task AsanaTask) model.Node {
	node := model.Node{}
	node.Title = task.Name
	node.DateAdded = task.CreatedAt
	node.Done = task.Completed
	node.Tags = task.Tags
	node.Tags = append(node.Tags, "asana")
	node.Ext = map[string]any{}
	node.Ext["asana"] = task
	node.Description = task.Notes
	return node
}
