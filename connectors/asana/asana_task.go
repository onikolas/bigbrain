package asana

import "time"

// A type into which to unmarhal tasks from asana
type AsanaTask struct {
	Gid       string    `json:"gid"`
	Completed bool      `json:"completed"`
	Name      string    `json:"name"`
	CreatedAt time.Time `json:"created_at"`
	Notes     string    `json:"notes"`
	Tags      []string  `json:"tags"`
}

type AsanaTasksPage struct {
	Data     []AsanaTask `json:"data"`
	NextPage struct {
		Offset string `json:"offset"`
		Path   string `json:"path"`
		Uri    string `json:"uri"`
	} `json:"next_page"`
}

type AsanaAuthorizeResponse struct {
	Data []struct {
		Gid          string `json:"gid"`
		Name         string `json:"name"`
		ResourceType string `json:"resource_type"`
	} `json:"data"`
}
