package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"os"

	"gitlab.com/onikolas/bigbrain/model"
	"gopkg.in/yaml.v3"
)

var Settings struct {
	Port      int
	AsanaUser string
	AsanaPam  string
}

func main() {
	// get app root from flags or use default
	root := flag.String("r", "", "Root directory where bigbrain files are stored")
	flag.Parse()
	if *root == "" {
		home, _ := os.UserHomeDir()
		*root = home + "/.bigbrain"
	}
	fmt.Println("Bigbrain storage at: ", *root)

	// load settings
	bytes, err := ioutil.ReadFile(*root + "/settings.yaml")
	if err != nil {
		panic(err.Error())
	}

	if err = yaml.Unmarshal(bytes, &Settings); err != nil {
		panic(err.Error())
	}

	// init db
	model := model.Model{}
	if err := model.Init(*root); err != nil {
		panic(err)
	}

	server := Server{}
	server.Init(&model)
}
