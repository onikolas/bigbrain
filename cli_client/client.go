package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"
	"os/exec"
	"sort"
	"strings"
	"time"

	"github.com/peterh/liner"
	"gitlab.com/onikolas/bigbrain/model"
	"golang.org/x/exp/slices"
)

var Settings struct {
	ServerUrl string
}

func main() {

	Settings.ServerUrl = os.Getenv("BIGBRAIN_URL")
	if Settings.ServerUrl == "" {
		Settings.ServerUrl = "http://localhost:16661"
	}

	if len(os.Args) == 1 {
		addNoteCmd()
	}

	if len(os.Args) >= 2 {
		switch os.Args[1] {
		case "notUsed":
			addNoteWithEditor()
		case "delete":
			deleteCmd(os.Args[2:])
		case "done":
			doneCmd(os.Args[2:])
		case "get":
			getCmd(os.Args[2:])
		case "list":
			listCmd(os.Args[2:])
		case "edit":
			editCmd(os.Args[2:])
		case "sync":
			syncCmd()
		case "help":
			fmt.Println("Usage: bbc [None/get/edit/delete/done]")
		}
	}
}

func addNoteCmd() {
	node := model.Node{}
	line := liner.NewLiner()
	line.SetCtrlCAborts(true)
	defer line.Close()

	if str, err := line.Prompt("Title: "); err == nil {
		node.Title = str
	}

	if str, err := line.Prompt("Deadline (like 02 Jan 06 15:00 EET): "); err == nil {
		if str != "" {
			node.Deadline, err = time.Parse(time.RFC822, str)
			if err != nil {
				panic(err)
			}
		}
	}

	if str, err := line.Prompt("Effort Estimate: "); err == nil {
		node.EffortEstimate, err = time.ParseDuration(str)
		if err != nil {
			node.EffortEstimate = 0
		}
	}

	if str, err := line.Prompt("Description: "); err == nil {
		node.Description = str
	}

	if str, err := line.Prompt("Tags (comma separated): "); err == nil {
		node.Tags = strings.Split(str, ",")
	}

	if str, err := line.Prompt("File link: "); err == nil {
		node.FileLink = str
	}

	node.DateAdded = time.Now()

	nodeBytes, _ := json.Marshal(node)

	resp, err := http.Post(Settings.ServerUrl+"/add", "application/json", bytes.NewReader(nodeBytes))
	if err != nil {
		fmt.Println(err)
	}
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	fmt.Println(string(body))
}

func addNoteWithEditor() {
	// get a note template from the server
	resp, err := http.Get(Settings.ServerUrl + "/template")
	if err != nil {
		fmt.Println(err)
		return
	}
	defer resp.Body.Close()

	filename := os.TempDir() + "/temp-note.json"
	file, err := os.Create(filename)
	if err != nil {
		fmt.Println(err)
		return
	}

	body, err := io.ReadAll(resp.Body)
	file.Write(body)
	file.Close()

	// open template in editor
	cmd := exec.Command("emacs", filename)
	var out bytes.Buffer
	cmd.Stdout = &out
	if err := cmd.Start(); err != nil {
		panic(err)
	}
	fmt.Println("Waiting for editor...")
	cmd.Wait()
	file, _ = os.Open(filename)

	// send note to server
	resp2, err := http.Post(Settings.ServerUrl+"/add", "application/json", file)
	if err != nil {
		fmt.Println(err)
	}
	defer resp2.Body.Close()
	body2, err := io.ReadAll(resp2.Body)
	fmt.Println(string(body2))
}

func deleteCmd(args []string) {
	if len(args) == 0 {
		line := liner.NewLiner()
		line.SetCtrlCAborts(true)
		defer line.Close()
		if str, err := line.Prompt("Delete UUID: "); err == nil {
			http.Get(Settings.ServerUrl + "/delete?id=" + str)
		}
	} else {
		http.Get(Settings.ServerUrl + "/delete?id=" + args[0])
	}
}

func doneCmd(args []string) {
	if len(args) == 0 {
		line := liner.NewLiner()
		line.SetCtrlCAborts(true)
		defer line.Close()
		if str, err := line.Prompt("Mark done UUID: "); err == nil {
			http.Get(Settings.ServerUrl + "/markdone?id=" + str)
		}
	}
	http.Get(Settings.ServerUrl + "/markdone?id=" + args[0])
}

func getCmd(args []string) {
	var uid string
	if len(args) == 0 {
		line := liner.NewLiner()
		line.SetCtrlCAborts(true)
		defer line.Close()
		if str, err := line.Prompt("Get UUID: "); err == nil {
			uid = str
		}
	} else {
		uid = args[0]
	}

	resp, err := http.Get(Settings.ServerUrl + "/get?id=" + uid)
	fmt.Println(Settings.ServerUrl + "/get?id=" + uid)
	if err != nil {
		fmt.Println(err)
	}
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	fmt.Println(string(body))
}

func editCmd(args []string) {
	var uid string
	if len(args) == 0 {
		line := liner.NewLiner()
		line.SetCtrlCAborts(true)
		defer line.Close()
		if str, err := line.Prompt("Get UUID: "); err == nil {
			uid = str
		}
	} else {
		uid = args[0]
	}

	resp, err := http.Get(Settings.ServerUrl + "/get?id=" + uid)
	filename := os.TempDir() + "/temp-note.json"
	file, err := os.Create(filename)
	if err != nil {
		fmt.Println(err)
		return
	}

	body, err := io.ReadAll(resp.Body)
	file.Write(body)
	file.Close()

	// open template in editor
	cmd := exec.Command("emacs", filename)
	var out bytes.Buffer
	cmd.Stdout = &out
	if err := cmd.Start(); err != nil {
		panic(err)
	}
	fmt.Println("Waiting for editor...")
	cmd.Wait()
	file, _ = os.Open(filename)

	// send note to server
	resp2, err := http.Post(Settings.ServerUrl+"/replace", "application/json", file)
	if err != nil {
		fmt.Println(err)
	}
	defer resp2.Body.Close()
	body2, err := io.ReadAll(resp2.Body)
	fmt.Println(string(body2))
}

func listCmd(args []string) {
	resp, _ := http.Get(Settings.ServerUrl + "/list")
	body, _ := io.ReadAll(resp.Body)
	nodes := []model.Node{}
	json.Unmarshal(body, &nodes)

	sort.Slice(nodes, func(i, j int) bool {
		return nodes[i].Deadline.After(nodes[j].Deadline)
	})

	colorReset := "\033[0m"
	titleColor := "\033[34m"
	pastDueColor := "\033[31m"
	bold := "\033[1m"
	underline := "\033[4m"
	faint := "\033[2m"

	tags := []string{}
	ti := -1
	if slices.Contains(args, "tags") {
		ti = slices.Index(args, "tags")
	}
	if slices.Contains(args, "tags-all") {
		ti = slices.Index(args, "tags-all")
	}
	if ti >= 0 && len(args) > ti+1 {
		tags = args[ti+1:]
		fmt.Println(faint, "Filter tags: ", tags, colorReset)
	}

	for _, n := range nodes {
		// default behaviour is to skip tasks without deadline
		if n.Deadline.IsZero() && !slices.Contains(args, "all") {
			continue
		}

		// only show notes that have at least one tag
		if slices.Contains(args, "tags") {
			hit := false
			for _, tag := range tags {
				if slices.Contains(n.Tags, tag) {
					hit = true
				}
			}
			if !hit {
				continue
			}
		}
		// only show notes that have all requested tags
		if slices.Contains(args, "tags-all") {
			hit := true
			for _, tag := range tags {
				if !slices.Contains(n.Tags, tag) {
					hit = false
				}
			}
			if !hit {
				continue
			}
		}

		if !n.Deadline.IsZero() && n.Deadline.Before(time.Now()) {
			fmt.Print(bold, pastDueColor, underline, n.Title, colorReset)
		} else {
			fmt.Print(bold, titleColor, underline, n.Title, colorReset)
		}
		fmt.Printf("%s (%v) %s\n", faint, n.ID, colorReset)
		if n.Description != "" {
			fmt.Println(n.Description)
		}
		fmt.Print(titleColor, "Added: ", colorReset, n.DateAdded.Format(time.RFC822))
		if !n.Deadline.IsZero() {
			fmt.Print(titleColor, " Deadline: ", colorReset, n.Deadline.Format(time.RFC822))
		}
		fmt.Println(titleColor, " Effort: ", colorReset, n.EffortEstimate)
		fmt.Print(titleColor, "Tags: ", colorReset, n.Tags, "\n")
		fmt.Println("")
	}
}

func syncCmd() {
	http.Get(Settings.ServerUrl + "/asanasync")
}
