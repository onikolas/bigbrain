module gitlab.com/onikolas/bigbrain/bbc

go 1.19

require (
	github.com/peterh/liner v1.2.2
	gitlab.com/onikolas/bigbrain v0.0.0-20221108105440-187527c6ad1e
)

require (
	github.com/google/uuid v1.3.0 // indirect
	github.com/mattn/go-runewidth v0.0.3 // indirect
	golang.org/x/exp v0.0.0-20221126150942-6ab00d035af9 // indirect
	golang.org/x/sys v0.1.0 // indirect
)
